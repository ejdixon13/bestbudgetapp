import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { MonthYear } from '../models';


@Component({
    selector: 'month-item',
    templateUrl: 'month-item.component.html',
    styleUrls: ['month-item.component.scss']
})

export class MonthItemComponent implements OnInit {
    @Input() monthYear: MonthYear;
    @Input() isSelected: boolean;

    @Output() select = new EventEmitter<MonthYear>();
    constructor() { }

    ngOnInit() { }

    onSelect() {
        this.select.emit(this.monthYear);
    }
}