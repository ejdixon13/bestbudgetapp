import { Component, OnInit } from '@angular/core';
import { PopoverController, BooleanValueAccessor, NavParams } from '@ionic/angular';

export interface AvailableOptions {
	addNote?: boolean;
	forgetTransaction?: boolean;
	addBudgetItem?: boolean;
	editCategory?: boolean;
	deleteCategory?: boolean;
}

@Component({
    selector: 'options',
    templateUrl: 'options.component.html'
})
export class OptionsComponent implements OnInit {

	availableOptions: AvailableOptions;

	constructor(private popoverCtrl: PopoverController, public navParams: NavParams) {
		this.availableOptions = this.navParams.get('availableOptions');
	 }

	addNote() {
		this.popoverCtrl.dismiss({
			addNote: true
		});
	}
	forgetTransaction() {
		this.popoverCtrl.dismiss({
			forgetTransaction: true
		});
	}
	addBudgetItem() {
		this.popoverCtrl.dismiss({
			addBudgetItem: true
		});
	}
	deleteCategory() {
		this.popoverCtrl.dismiss({
			deleteCategory: true
		});
	}
	editCategory() {
		this.popoverCtrl.dismiss({
			editCategory: true
		});
	}
    ngOnInit() { }
}
