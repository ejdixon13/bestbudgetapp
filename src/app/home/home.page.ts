import { Component, ViewChild, OnInit, OnDestroy } from '@angular/core';
import { AppService, BankReloginStatus, PublicTokenResponse } from '../app.service';
import { PickerController, AlertController } from '@ionic/angular';
import { PickerOptions, PickerColumn, PickerColumnOption } from '@ionic/core';
import { Transaction, TextValue, Category, LineItem, MonthYear } from './models';
import { MONTH_OPTIONS, FULL_MONTH_NAME } from './consts';
import { findIndex, find, unique, remove } from 'lodash';
import { DragulaService } from 'ng2-dragula';
import { BudgetItemComponent } from './budget-item/budget-item.component';
import { CurrencyPipe } from '@angular/common';
import { Subscription } from 'rxjs';
import { tap } from 'rxjs/operators';

declare var Plaid;
const PLAID_PUBLISHABLE_KEY = 'cee8d57a3594e6e0f8f30d7d884618';   // Replace this with your own values
const PLAID_ENVIRONMENT = 'development';
const APP_NAME = 'Best_Budget_App'; // Replace this with your own values
const TRANSACTION_BUBBLES = 'TRANSACTION_BUBBLES';
const TRANSACTIONS = 'TRANSACTIONS';
const getMonth = (monthValue: string) => {
  return MONTH_OPTIONS.find(opt => opt.value === parseInt(monthValue, 10));
};
const today = new Date();
const THIS_MONTH: () => MonthYear = () => ({
  month: getMonth(today.getMonth().toString()),
  year: today.getFullYear()
});
const NEXT_MONTH: () => MonthYear = () => {
  const dateCopy = new Date(today.getTime());
  dateCopy.setMonth(dateCopy.getMonth() + 1, 1);
  return {
    month: getMonth(dateCopy.getMonth().toString()),
    year: dateCopy.getFullYear()
  };
};


const uniqueId = () => Math.random().toString(36).substring(2)
  + (new Date()).getTime().toString(36);

@Component({
  selector: 'app-home',
  templateUrl: 'home.page.html',
  styleUrls: ['home.page.scss'],
})
export class HomePage implements OnInit, OnDestroy {

  private linkHandler;
  firebaseData: any;
  transactions: Transaction[];
  picker: any;
  categories: Category[] = [];
  dropModelSubscription: Subscription;
  dragModelSubscription: Subscription;

  showBubbles = false;
  showMonthYearPicker = false;
  selectableMonthYears: MonthYear[];
  selectedMonthYear: MonthYear;
  disableScroll = false;
  showPublicTokenField = false;
  showReLink = false;
  bankAccessToken: string;
  readyToLinkAccount: boolean;
  readonly FULL_MONTH_NAME = FULL_MONTH_NAME;

  constructor(private appService: AppService,
    private pickerCtrl: PickerController,
    private alertController: AlertController,
    private currencyPipe: CurrencyPipe,
    private dragulaService: DragulaService) {
    this.dragulaService.createGroup(TRANSACTIONS, {
      moves: (el, container, handle) => {
        return handle.className.includes('transaction-handle');
      }
    });
    this.dragModelSubscription = this.dragulaService.drag(TRANSACTIONS)
      .subscribe(() => {
        this.disableScroll = true;
      });
    this.dropModelSubscription = this.dragulaService.dropModel(TRANSACTIONS)
      .subscribe(({ name, el, target, source, sibling, sourceModel, targetModel, item }) => {
        this.disableScroll = false;
        const targetIds = target.id.split('+');
        const sourceIds = source.id.split('+');
        if (targetIds[0] === TRANSACTION_BUBBLES && sourceIds[0] === TRANSACTION_BUBBLES) {
          return;
        }
        if (targetIds[0] === TRANSACTION_BUBBLES) {
          this.showBubbles = false;
          const transactionId = item.transaction_id;
          const categoryId = this.getCategoryIdFromTransactionId(transactionId);
          const lineItemId = this.getLineItemIdFromTransactionId(transactionId);
          this.appService.getTransactionRef(this.selectedMonthYear, categoryId, lineItemId)
            .child(item.id)
            .remove();
          setTimeout(() => this.showBubbles = true, 100);
        } else {
          const targetCategoryId = targetIds[0];
          const targetLineItemId = targetIds[1];
          const sourceCategoryId = sourceIds[0];
          const sourceLineItemId = sourceIds[1];
          item.note = item.note ? item.note : '';
          if ((sourceLineItemId !== targetLineItemId)) {
            this.appService
              .saveTransaction(this.selectedMonthYear, targetCategoryId, targetLineItemId, item);
            if ( sourceCategoryId !== TRANSACTION_BUBBLES) {
              this.appService.getTransactionRef(this.selectedMonthYear, sourceCategoryId, sourceLineItemId)
              .child(item.id)
              .remove();
            }
          }
        }
      });
  }

  private getCategoryIdFromTransactionId(transactionId: string) {
    const category: Category = this.categories
      .find(c => c.items.some(i => i.transactions.some(t => t ? t.transaction_id === transactionId : false)));
    return category ? category.id : null;
  }
  private getLineItemIdFromTransactionId(transactionId: string) {
    let lineItem: LineItem;
      this.categories.forEach(c => {
        lineItem = c.items.find(i => i.transactions.some(t => t ? t.transaction_id === transactionId : false));
      });
    return lineItem ? lineItem.id : null;
  }


  get totalIncome(): number {
    if (this.categories.length) {
      const incomeCategory = this.categories.find(c => c.id === 'INCOME');
      const incomeLineItems = incomeCategory.items;
      return incomeLineItems ? incomeLineItems.map(i => i.amount).reduce((p, c) => (+p) + (+c), 0) : 0;
    }
    return 0;
  }


  get remainingAmount(): number {
    if (this.categories) {
      const allCategoriesExceptIncome = this.categories.filter(c => c.id !== 'INCOME');
      const totalTransactionAmounts = allCategoriesExceptIncome
        .map(
          cat => cat.items ? cat.items
            .map(i => i.transactions ? i.transactions.map(t => t ? t.amount : 0).reduce((p, c) => (+p) + (+c), 0) : 0)
            .reduce((p, c) => (+p) + (+c), 0) : 0
        )
        .reduce((p, c) => (+c) + (+p), 0);
      return this.totalIncome - totalTransactionAmounts;
    }
    return 0;
  }

  get leftToBudget(): number {
    if (this.categories) {
      const allCategoriesExceptIncome = this.categories.filter(c => c.id !== 'INCOME');
      const budgetedAmount = allCategoriesExceptIncome
        .map(cat => cat.items ? cat.items.map(i => i.amount).reduce((p, c) => (+p) + (+c), 0) : 0)
        .reduce((p, c) => (+c) + (+p), 0);
      return this.totalIncome - budgetedAmount;
    }
    return 0;
  }

  get bankReloginStatus(): BankReloginStatus {
    return this.appService.bankReloginStatus;
  }


  getOrderedTransactions(transactions: Transaction[] = []) {
    return transactions.sort((a, b) => {
      if (a.date > b.date) {
        return -1;
      } else if (a.date < b.date) {
        return 1;
      } else {
        return 0;
      }
    });
  }

  getTransactions(monthYear: MonthYear) {
    const firstDay = new Date(monthYear.year, monthYear.month.value, 1).toISOString().substr(0, 10);
    const lastDay = new Date(monthYear.year, monthYear.month.value + 1, 0).toISOString().substr(0, 10);
    return this.appService.getTransactions(firstDay, lastDay);
  }

  updateSelectableMonthYears() {
    this.selectableMonthYears = [
      THIS_MONTH(),
      NEXT_MONTH()
    ];
    const isAddedToSelectableMonths = (m: MonthYear) => {
      return this.selectableMonthYears.some(option => (option.month.value === m.month.value) && (option.year === m.year));
    };
    this.appService.getBudgetRef().once('value', snapshot => {
      if (snapshot.val()) {
        const budgets = snapshot.val();
        for (const budgetYear in budgets) {
          if (budgets.hasOwnProperty(budgetYear)) {
            const budgetMonths = budgets[budgetYear];
            for (const budgetMonth in budgetMonths) {
              if (budgetMonths.hasOwnProperty(budgetMonth)) {
                const monthToAdd = {
                  year: parseInt(budgetYear, 10),
                  month: getMonth(budgetMonth)
                };
                if (!isAddedToSelectableMonths(monthToAdd)) {
                  this.selectableMonthYears.push(monthToAdd);
                }
              }
            }
          }
        }
      }
      this.selectableMonthYears.sort((a, b) => {
        if (a.month.value >= b.month.value && a.year >= b.year) {
          return 1;
        } else {
          return -1;
        }
      });
    });
  }

  onSelectMonthYear(monthYear: MonthYear) {
    this.selectedMonthYear = monthYear;
    this.updateTransactions(this.selectedMonthYear);
    this.addInitialIncomeCategoryIfDoesNotExist();
    this.showMonthYearPicker = false;
  }

  isSelectedMonthYear(monthYear: MonthYear) {
    return (this.selectedMonthYear.month.value === monthYear.month.value) && (this.selectedMonthYear.year === monthYear.year);
  }

  async forgetTransaction(transaction: Transaction) {
    const alert = await this.alertController.create({
      header: 'Are you sure yo want to forget ' + transaction.name + '?',
      buttons: [
        {
          text: 'Cancel',
          role: 'cancel',
          cssClass: 'secondary',
          handler: () => {
            console.log('Confirm Cancel');
          }
        }, {
          text: 'Forget',
          cssClass: 'danger',
          handler: async () => {
            await this.appService
              .addToForgottenTransactions(this.selectedMonthYear, transaction);
            this.consolidateForgottenTransactions();
          }
        }
      ]
    });

    await alert.present();
  }

  updateTransactions(monthYear: MonthYear) {
    this.getTransactions(monthYear)
      .subscribe(data => {
        this.transactions = data;
        this.updateTransactionsBasedOnCategorizedTransactions();
        this.consolidateForgottenTransactions();
        this.checkBankReloginStatus();
      },
        (data) => {
          console.log(data);
        },
        () => {
          console.log('end');
        });
  }

  private updateTransactionsBasedOnCategorizedTransactions() {
    this.appService.getCategoryRef(this.selectedMonthYear).once('value', snapshot => {
      this.categories = this.mapToCategories(snapshot.val());
      this.categories.forEach(cat => {
        cat.items.forEach(item => {
          item.transactions.forEach(t => {
            if (t) {
              remove(this.transactions,
                tran => (tran.transaction_id === t.transaction_id) ||
                  (tran.pending_transaction_id === t.transaction_id)
              );
              this.transactions
                .filter(tran => this.isTransactionPossibleDuplicate(tran, t))
                .forEach(trans => trans.possibleDuplicate = true);
            }
          });
        });
      });
    });
  }

  private isTransactionPossibleDuplicate(transaction: Transaction, transactionToCompare: Transaction) {
    return (transaction.name === transactionToCompare.name) && (Math.abs(transaction.amount) === Math.abs(transactionToCompare.amount));
  }

  private consolidateForgottenTransactions() {
    this.appService.getForgottenTransactionsForMonthYear(this.selectedMonthYear)
      .once('value', snapshot => {
        const forgottenTransactions = snapshot.val();
        for (const t in forgottenTransactions) {
          if (forgottenTransactions.hasOwnProperty(t)) {
            remove(this.transactions, tran => tran.transaction_id === forgottenTransactions[t].transaction_id);
          }
        }
      });
  }

  private checkBankReloginStatus() {
    if (this.bankReloginStatus.capitalOne) {
      this.appService.getPlaidPublicToken()
        .pipe(
          tap((publicToken: string) => this.reconfigurePlaidAccount(publicToken))
        ).subscribe(() => this.readyToLinkAccount = true);
    }
  }

  getPlaidPublicToken() {
    return this.appService.getPlaidPublicToken()
      .subscribe((token: string) => {
        console.log(token);
      } );
  }

  ionViewDidEnter() {
    // this.configurePlaid();
    // this.reconfigurePlaidAccount();
  }

  configurePlaid() {
    this.linkHandler = Plaid.create({
      clientName: APP_NAME,
      env: PLAID_ENVIRONMENT,
      key: PLAID_PUBLISHABLE_KEY,
      product: ['auth', 'transactions'],
      selectAccount: true,
      forceIframe: true,
      onLoad: function () {
        console.log('loaded');
      },
      onSuccess: function (public_token: string, metadata: any) {
        console.log(metadata);
      },
      onExit: function (err, metadata: any) {
        if (err != null) {
          console.error(err);
        }
      }
    });
  }

  /**
   * Use this instead of configurePlaidAccount when you get errorCode ITEM_LOGIN_REQUIRED
   * add in public_token for item
   */
  reconfigurePlaidAccount(publicToken: string) {
    // this.appService.getPublicToken(accessToken)
    //   .subscribe(data => { });
        this.linkHandler = Plaid.create({
          clientName: APP_NAME,
          env: PLAID_ENVIRONMENT,
          key: PLAID_PUBLISHABLE_KEY,
          product: ['auth', 'transactions'],
          token: publicToken,
          onLoad: function () {
            console.log('loaded');
          },
          onSuccess: function (public_token: string, metadata: any) {
            console.log(metadata);
            this.showReLink = true;
          },
          onExit: function (err, metadata: any) {
            if (err != null) {
              console.error(err);
            }
          }
        });
  }

  forceExitLink() {
    this.linkHandler.exit();
  }

  link() {
    this.linkHandler.open();
  }

  async openCategoryPrompt(category: Category) {
    let header = 'Add New Category';
    const nameInput: any = {
      name: 'category',
      type: 'text',
      placeholder: 'Category'
    };
    if (category) {
      nameInput.value = category.name;
      header = 'Edit Category';
    }
    const alert = await this.alertController.create({
      header: header,
      inputs: [
        nameInput
      ],
      buttons: [
        {
          text: 'Cancel',
          role: 'cancel',
          cssClass: 'secondary',
          handler: () => {
            console.log('Confirm Cancel');
          }
        }, {
          text: 'Save',
          handler: (value) => {
            if (category) {
              this.appService
                .getCategoryRef(this.selectedMonthYear)
                .child(category.id)
                .update({
                  name: value.category
                });
            } else {
              this.appService.saveCategory(this.selectedMonthYear, { name: value.category });
            }
            console.log('Confirm Ok: ' + value.category);
          }
        }
      ]
    });

    await alert.present();
  }

  editBudgetItem(category: Category, budgetItemToEdit: LineItem) {
    this.openBudgetItemPrompt(category, budgetItemToEdit);
  }

  async openBudgetItemPrompt(category: Category, budgetItemToEdit: LineItem) {
    let header = 'Add New Budget Item';
    const nameInput: any = {
      name: 'name',
      type: 'text',
      placeholder: 'Name'
    };
    const amountInput: any = {
      name: 'amount',
      type: 'number',
      placeholder: 'Budget Amount'
    };
    if (budgetItemToEdit) {
      nameInput.value = budgetItemToEdit.name;
      amountInput.value = budgetItemToEdit.amount;
      header = 'Edit Budget Item';
    }
    const getSubHeaderText = () => 'Left to budget: ' + this.currencyPipe.transform(this.leftToBudget.toString());
    const alert = await this.alertController.create({
      header: header,
      subHeader: getSubHeaderText(),
      inputs: [
        nameInput,
        amountInput
      ],
      buttons: [
        {
          text: 'Cancel',
          role: 'cancel',
          cssClass: 'secondary',
          handler: () => {
            console.log('Confirm Cancel');
          }
        }, {
          text: 'Save',
          handler: (value) => {
            if (budgetItemToEdit) {
              const itemToSave = find(category.items, item => item.id === budgetItemToEdit.id);
              itemToSave.name = value.name;
              itemToSave.amount = value.amount;
              this.appService
                .getLineItemRef(this.selectedMonthYear, category.id)
                .child(budgetItemToEdit.id)
                .update({
                  name: value.name,
                  amount: value.amount
                });
            } else {
              this.appService
                .saveLineItem(this.selectedMonthYear, category.id, {
                  name: value.name,
                  amount: value.amount
                });
            }
          }
        }
      ]
    });

    await alert.present();
  }

  async openAddNotePrompt(transactionId: string) {
    const alert = await this.alertController.create({
      header: 'Add Note To Transaction',
      inputs: [
        {
          name: 'note',
          type: 'text',
          placeholder: 'Add Info'
        }
      ],
      buttons: [
        {
          text: 'Cancel',
          role: 'cancel',
          cssClass: 'secondary',
          handler: () => {
            console.log('Confirm Cancel');
          }
        }, {
          text: 'Add Note',
          handler: (value) => {
            this.transactions.find(t => t.transaction_id === transactionId).note = value.note;
          }
        }
      ]
    });

    await alert.present();
  }

  async deleteBudgetItem(category: Category, budgetItemToDelete: LineItem) {
    const alert = await this.alertController.create({
      header: 'Are you sure yo want to delete ' + budgetItemToDelete.name + '?',
      buttons: [
        {
          text: 'Cancel',
          role: 'cancel',
          cssClass: 'secondary',
          handler: () => {
            console.log('Confirm Cancel');
          }
        }, {
          text: 'Delete',
          cssClass: 'danger',
          handler: () => {
            this.appService
              .getLineItemRef(this.selectedMonthYear, category.id)
              .child(budgetItemToDelete.id)
              .remove();
          }
        }
      ]
    });

    await alert.present();
  }

  async deleteCategory(category: Category) {
    const alert = await this.alertController.create({
      header: 'Are you sure yo want to delete ' + category.name + '?',
      buttons: [
        {
          text: 'Cancel',
          role: 'cancel',
          cssClass: 'secondary',
          handler: () => {
            console.log('Confirm Cancel');
          }
        }, {
          text: 'Delete',
          cssClass: 'danger',
          handler: () => {
            this.appService
              .getCategoryRef(this.selectedMonthYear)
              .child(category.id)
              .remove();
          }
        }
      ]
    });

    await alert.present();
  }

  copyBudgetFromLastMonth() {
    const lastMonthDate = new Date(this.selectedMonthYear.year, this.selectedMonthYear.month.value - 1, 1);
    const lastMonthRelativeToSelected: MonthYear = {
      month: getMonth(lastMonthDate.getMonth().toString()),
      year: lastMonthDate.getFullYear()
    };
    this.appService.getCategoryRef(lastMonthRelativeToSelected)
      .once('value', snapshot => {
        if (snapshot.val()) {
          console.log(snapshot.val());
          this.appService.overwriteCategories(this.selectedMonthYear, snapshot.val(), true);
        }
      });
  }

  ngOnInit() {
    this.selectedMonthYear = THIS_MONTH();
    this.updateSelectableMonthYears();
    this.updateTransactions(this.selectedMonthYear);
    this.addInitialIncomeCategoryIfDoesNotExist();
    this.appService.getCategoryRef(this.selectedMonthYear).on('value', snapshot => {
      this.categories = this.mapToCategories(snapshot.val());
    });
  }

  addInitialIncomeCategoryIfDoesNotExist() {
    this.appService.getIncomeCategoryRef(this.selectedMonthYear)
    .once('value',
      snapshot => {
        if (!snapshot.val()) {
          this.appService.saveInitialIncomeCategory(this.selectedMonthYear);
        }
      });
  }
  private mapToCategories(dbCategories: any = {}) {
    const categories = [];
    for (const category in dbCategories) {
      if (dbCategories.hasOwnProperty(category)) {
        categories.push({
          id: category,
          name: dbCategories[category].name,
          items: this.mapToLineItems(dbCategories[category].items)
        });
      }
    }
    return categories.sort((a, b) => {
      if (a.id === 'INCOME') {
        return -2;
      } else {
        return 0;
      }
    });
  }

  private mapToLineItems(dbItems: any = {}) {
    const lineItems: LineItem[] = [];
    for (const item in dbItems) {
      if (dbItems.hasOwnProperty(item)) {
        lineItems.push({
          id: item,
          name: dbItems[item].name,
          amount: dbItems[item].amount,
          transactions: this.mapToTransactions(dbItems[item].transactions)
        });
      }
    }
    return lineItems;
  }
  private mapToTransactions(dbTransactions: any = {}) {
    const transactions: Transaction[] = [];
    for (const transaction in dbTransactions) {
      if (dbTransactions.hasOwnProperty(transaction)) {
        transactions.push({
          id: transaction,
          transaction_id: dbTransactions[transaction].transaction_id,
          name: dbTransactions[transaction].name,
          amount: dbTransactions[transaction].amount,
          date: dbTransactions[transaction].date,
          category: dbTransactions[transaction].category,
          category_id: dbTransactions[transaction].category_id,
          pending: dbTransactions[transaction].pending,
          note: dbTransactions[transaction].note
        });
      }
    }
    return transactions;
  }

  ngOnDestroy() {
    this.dropModelSubscription.unsubscribe();
  }

}
