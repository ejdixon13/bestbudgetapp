import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { PopoverController } from '@ionic/angular';
import { OptionsComponent, AvailableOptions } from '../options/options.component';
import { Category } from '../models';

@Component({
    selector: 'category',
    templateUrl: 'category.component.html',
    styleUrls: ['category.component.scss'],
})

export class CategoryComponent implements OnInit {
    @Input() category: Category;
    @Output() addLineItem = new EventEmitter();
    @Output() editCategory = new EventEmitter();
    @Output() deleteCategory = new EventEmitter();
    constructor(private popoverController: PopoverController) { }

    isIncome() {
        return this.category.id === 'INCOME';
    }

    async presentOptions(ev: any) {
        const availableOptions: AvailableOptions = {
            addBudgetItem: true,
            deleteCategory: !this.isIncome(),
            editCategory: true
        };
        const popover = await this.popoverController.create({
            component: OptionsComponent,
            componentProps: {
                'availableOptions': availableOptions
            },
            event: ev,
            translucent: true,
            showBackdrop: false
        });
        popover.onDidDismiss().then(optionResponse => {
            if (optionResponse.data) {
                if (optionResponse.data.addBudgetItem) {
                    this.addLineItem.emit(null);
                } else if (optionResponse.data.deleteCategory) {
                    this.deleteCategory.emit(null);
                } else if (optionResponse.data.editCategory) {
                    this.editCategory.emit(null);
                }
            }
        });
        return await popover.present();
    }
    ngOnInit() { }
}
