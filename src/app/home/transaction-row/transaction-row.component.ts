import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { Transaction } from '../models';

@Component({
    selector: 'transaction-row',
    templateUrl: 'transaction-row.component.html',
    styleUrls: ['transaction-row.component.scss'],
})

export class TransactionRowComponent implements OnInit {
    @Input() transaction: Transaction;
    constructor() { }

    ngOnInit() { }
}
