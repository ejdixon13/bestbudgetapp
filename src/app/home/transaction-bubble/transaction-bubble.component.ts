import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { Transaction } from '../models';
import { PopoverController } from '@ionic/angular';
import { OptionsComponent, AvailableOptions } from '../options/options.component';

@Component({
    selector: 'transaction-bubble',
    templateUrl: 'transaction-bubble.component.html',
    styleUrls: ['transaction-bubble.component.scss'],
})

export class TransactionBubbleComponent implements OnInit {
    @Input() transaction: Transaction;
    @Output() forgetTransaction = new EventEmitter<string>();
    @Output() addNoteToTransaction = new EventEmitter<string>();
    activated = false;
    constructor(public popoverController: PopoverController) {}

    async presentPopover(ev: any) {
        const availableOptions: AvailableOptions = {
            addNote: true,
            forgetTransaction: true
        };
        const popover = await this.popoverController.create({
            component: OptionsComponent,
            componentProps: {
                'availableOptions': availableOptions
            },
            event: ev,
            translucent: true,
            showBackdrop: false
        });
        popover.onDidDismiss().then(optionResponse => {
            if (optionResponse.data) {
                if (optionResponse.data.forgetTransaction) {
                    this.forgetTransaction.emit(this.transaction.transaction_id);
                } else if (optionResponse.data.addNote) {
                    this.addNoteToTransaction.emit(this.transaction.transaction_id);
                }
            }
        });
        return await popover.present();
    }

    ngOnInit() { }
}
