import { NgModule } from '@angular/core';
import { CommonModule, CurrencyPipe } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { IonicModule } from '@ionic/angular';
import { RouterModule } from '@angular/router';

import { HomePage } from './home.page';
import { CategoryComponent } from './category/category.component';
import { BudgetItemComponent } from './budget-item/budget-item.component';
import { DragulaModule } from 'ng2-dragula';
import { TransactionRowComponent } from './transaction-row/transaction-row.component';
import { TransactionBubbleComponent } from './transaction-bubble/transaction-bubble.component';
import { OptionsComponent } from './options/options.component';
import { MonthItemComponent } from './month-item/month-item.component';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    RouterModule.forChild([
      {
        path: '',
        component: HomePage
      }
    ]),
    DragulaModule
  ],
  declarations: [
    HomePage,
    CategoryComponent,
    BudgetItemComponent,
    TransactionRowComponent,
    TransactionBubbleComponent,
    OptionsComponent,
    MonthItemComponent
  ],
  entryComponents: [
    OptionsComponent
  ],
  providers: [
    CurrencyPipe
  ]
})
export class HomePageModule {}
