import { TextValue } from './models';

export const MONTH_OPTIONS: TextValue[] = [
    {
        text: 'Jan',
        value: 0
    },
    {
        text: 'Feb',
        value: 1
    },
    {
        text: 'Mar',
        value: 2
    },
    {
        text: 'Apr',
        value: 3
    },
    {
        text: 'May',
        value: 4
    },
    {
        text: 'Jun',
        value: 5
    },
    {
        text: 'Jul',
        value: 6
    },
    {
        text: 'Aug',
        value: 7
    },
    {
        text: 'Sep',
        value: 8
    },
    {
        text: 'Oct',
        value: 9
    },
    {
        text: 'Nov',
        value: 10
    },
    {
        text: 'Dec',
        value: 11
    },
];

export const FULL_MONTH_NAME = {
    Jan: 'January',
    Feb: 'February',
    Mar: 'March',
    Apr: 'April',
    May: 'May',
    Jun: 'June',
    Jul: 'July',
    Aug: 'August',
    Sep: 'September',
    Oct: 'October',
    Nov: 'November',
    Dec: 'December'
};
