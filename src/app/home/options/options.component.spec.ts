import { TestBed, inject } from '@angular/core/testing';

import { OptionsComponent } from './options.component';

describe('a options component', () => {
	let component: OptionsComponent;

	// register all needed dependencies
	beforeEach(() => {
		TestBed.configureTestingModule({
			providers: [
				OptionsComponent
			]
		});
	});

	// instantiation through framework injection
	beforeEach(inject([OptionsComponent], (OptionsComponent) => {
		component = OptionsComponent;
	}));

	it('should have an instance', () => {
		expect(component).toBeDefined();
	});
});