import { Injectable } from '@angular/core';
import { Http, Headers } from '@angular/http';
import { of, Observable } from 'rxjs';
import {filter, map, switchMap} from 'rxjs/operators';
import { Transaction, Category, LineItem, MonthYear } from './home/models';

declare var firebase;
const FULL_SIMPLE_URL = 'https://signin.simple.com';
const AWS_PLAID_PASSTHROUGH = 'https://7p9aj6uvqd.execute-api.us-east-1.amazonaws.com/default/Passthrough';
const MONTH_PATH = (year: number, month: number) => year + '/' + month;
const CATEGORY_PATH = 'category';
const INCOME_CATEGORY = 'INCOME';
const LINE_ITEM = 'items';
const TRANSACTIONS = 'transactions';
const FORGOTTEN_TRANSACTIONS = 'forgotten_transactions';
const LOGIN_REQUIRED_ERROR_CODE = 'ITEM_LOGIN_REQUIRED';
const CAPITAL_ONE_BANK = 'capitalOne';
const SIMPLE_BANK = 'simple';

export interface PublicTokenResponse {
    expiration: string;
    public_token: string;
    request_id: string;
}

export interface BankReloginStatus {
    capitalOne: boolean;
    simple: boolean;
}
@Injectable()
export class AppService {

    bankReloginStatus: BankReloginStatus = {
        capitalOne: false,
        simple: false
    };

    constructor(private http: Http) {
        // Initialize Firebase
        const config = {
            apiKey: 'AIzaSyAUretfVcusLknhwtzY_J6lTz418hyEGDM',
            authDomain: 'best-budget-app.firebaseapp.com',
            databaseURL: 'https://best-budget-app.firebaseio.com',
            projectId: 'best-budget-app',
            storageBucket: 'best-budget-app.appspot.com',
            messagingSenderId: '405608790906'
        };
        firebase.initializeApp(config);
    }
    getBudgetRef = () => firebase.database().ref('budgets/');

    async authenticateSimpleHTTP() {
        // return await this.http.get(FULL_SIMPLE_URL, {}, {});
    }

    getFirebaseDatabase() {
        return firebase.database();
    }


    saveCategory(monthYear: MonthYear, category: Category) {
        this.getCategoryRef(monthYear)
            .push(category);
    }

    saveLineItem(monthYear: MonthYear, categoryId: string, lineItem: LineItem) {
        this.getLineItemRef(monthYear, categoryId)
            .push(lineItem);
    }
    saveTransaction(monthYear: MonthYear, categoryId: string, lineItemId: string, transaction: Transaction) {
        this.getTransactionRef(monthYear, categoryId, lineItemId)
            .push(transaction);
    }

    addToForgottenTransactions(monthYear: MonthYear, transaction) {
        return this.getMonthRef(monthYear)
            .child(FORGOTTEN_TRANSACTIONS)
            .push(transaction);
    }

    getForgottenTransactionsForMonthYear(monthYear: MonthYear) {
        return this.getMonthRef(monthYear).child(FORGOTTEN_TRANSACTIONS);
    }

    saveInitialIncomeCategory(monthYear: MonthYear) {
        this.getCategoryRef(monthYear)
            .child(INCOME_CATEGORY)
            .set({
                name: 'Income',
                items: []
            });
    }

    overwriteCategories(monthYear: MonthYear, dbCategories: any, doRemoveTransactions: boolean) {
        if (doRemoveTransactions) {
            dbCategories = this.removeTransactions(dbCategories);
        }
        this.getCategoryRef(monthYear)
            .set(dbCategories);
    }

    private removeTransactions(dbCategories: any = {}) {
        for (const cat in dbCategories) {
            if (dbCategories.hasOwnProperty(cat)) {
                const items = dbCategories[cat].items;
                for (const item in items) {
                    if (items.hasOwnProperty(item)) {
                        items[item].transactions = null;
                    }
                }
            }
        }
        return dbCategories;
    }

    getMonthRef(monthYear: MonthYear) {
        return this.getBudgetRef().child(MONTH_PATH(monthYear.year, monthYear.month.value));
    }

    getCategoryRef(monthYear: MonthYear) {
        return this.getMonthRef(monthYear).child(CATEGORY_PATH);
    }

    getLineItemRef(monthYear: MonthYear, categoryId: string) {
        return this.getCategoryRef(monthYear)
            .child(categoryId)
            .child(LINE_ITEM);
    }

    getTransactionRef(monthYear: MonthYear, categoryId: string, lineItemId: string) {
        return this.getLineItemRef(monthYear, categoryId)
            .child(lineItemId)
            .child(TRANSACTIONS);
    }

    getIncomeCategoryRef(monthYear: MonthYear) {
        return this.getCategoryRef(monthYear).child(INCOME_CATEGORY);
    }

    getTransactions(startDate: string, endDate: string): Observable<Transaction[]> {

        return this.http.get(AWS_PLAID_PASSTHROUGH, {
            params: {
                startDate: startDate,
                endDate: endDate
            }
        }).pipe(
            switchMap(data => {
                const simple = data.json().simple;
                const capitalOne = data.json().capitalOne;
                // const capitalOne = {
                //     'display_message': null,
                //     'error_code': 'ITEM_LOGIN_REQUIRED',
                //     'error_message': 'the login details... ',
                //     'error_type': 'ITEM_ERROR',
                //     'request_id': 'jbrZFFVareR31qy',
                //     'suggested_action': null
                // };
                this.bankReloginStatus.capitalOne = capitalOne.error_code === LOGIN_REQUIRED_ERROR_CODE;
                this.bankReloginStatus.simple = simple.error_code === LOGIN_REQUIRED_ERROR_CODE;
                const allTransactions = (simple.transactions as any[]).concat(capitalOne.transactions)
                    .filter(t => !!t); // ensure all transactions are defined
                return of(allTransactions);
            })
        );
    }

    // For Relogin
    getPlaidPublicToken(): Observable<string> {
        const headers = new Headers();
        headers.set('x-api-key', 'KPdCr6Mre48yEMZo045s4aHatFtTCC8s5Y63SQDo');
        return this.http.get('https://7p9aj6uvqd.execute-api.us-east-1.amazonaws.com/default/getPlaidPublicToken', {
            headers: headers
        }).pipe(
            switchMap((value: any) => of((value.json() as PublicTokenResponse).public_token))
        );
    }
}
