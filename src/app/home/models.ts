
export interface Transaction {
    id: string;
    transaction_id: string;
    name: string;
    date: string;
    amount: number;
    category: { [key: number]: string; }[];
    category_id: string;
    pending: boolean;
    note: string;
    possibleDuplicate?: boolean;
    pending_transaction_id?: string;
}

export interface TextValue {
    text: string;
    value: number;
}

export interface MonthYear {
    month: TextValue;
    year: number;
}

export interface Category {
    id?: string;
    name: string;
    items?: LineItem[];
}

export interface LineItem {
    id?: string;
    name: string;
    amount: number;
    transactions?: Transaction[];
}

