import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { Transaction } from '../models';

@Component({
    selector: 'budget-item',
    templateUrl: 'budget-item.component.html',
    styleUrls: ['budget-item.component.scss'],
})

export class BudgetItemComponent implements OnInit {
    @Input() name: string;
    @Input() isIncome: boolean;
    @Input() amount: number;
    @Input() transactions: Transaction[] = [];
    @Input() expanded: boolean;
    // tslint:disable-next-line
    @Output() onEdit = new EventEmitter();
    @Output() OnTopRowClick = new EventEmitter();

    showBottomLevel = false;

    constructor() { }

    get amountLeft() {
        if (!this.isIncome) {
            const sumOfAllTransactions = this.transactions.map(t => t ? t.amount : 0).reduce((p, c) => p + c, 0);
            return this.amount - sumOfAllTransactions;
        }
        return this.amount;
    }

    editButtonClick( event: any) {
        this.onEdit.emit(null);
        event.preventDefault();
        // handle IE click-through modal bug
        // event.stopPropagation();
    }

    ngOnInit() { }
}
